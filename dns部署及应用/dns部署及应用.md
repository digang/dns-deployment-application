## DNS安装
```

yum -y install bind bind-chroot
bind是DNS主程序包
bind-chroot DNS安全包,改变默认DNS目录，将DNS运行在监牢模式
安装路径
/etc/named.conf
/var/named/
CP文件
cp -p /etc/named.conf /var/named/chroot/etc/
cp -p /var/named/named.* /var/named/chroot/var/named/

```
## 配置主配置文件
```
vim /var/named/chroot/etc/named.conf
检查配置文件是否有错命令
named-checkconf /var/named/chroot/etc/named.conf 
启动DNS
systemctl restart named-chroot
systemctl enable named-chroot.service
```
## 区域数据库文件
```
vim /var/named/chroot/var/named/named.ca
正向解析模板文件named.empty
正向解析模板文件named.localhost
反向解析模板文件named.loopback
```
### 正向解析
```
配置主配置文件
/var/named/chroot/etc/named.conf
插入这几行
zone "9ihanhan.cn" IN {
type master;
file "9ihanhan.cn.zone";
};
配置区域数据库文件配置
/var/named/chroot/var/named
cp named.localhost love.9ihanhan.cn.zone 
chgrp named love.9ihanhan.cn.conf
修改这几行
$TTL 1D
love.9ihanhan.cn.       IN SOA  9ihanhan.cn. rname.invalid. (

                                        0       ; serial

                                        1D      ; refresh

                                        1H      ; retry

                                        1W      ; expire

                                        3H )    ; minimum

        NS      9ihanhan.cn.

love    A       47.115.79.9


```
## 检查配置文件是否正确
```
检查区域数据库文件是否正确命令：
named-checkzone love.9ihanhan.cn /var/named/chroot/var/named/love.9ihanhan.cn.zone 
检查主配置文件是否正确命令
named-checkconf /var/named/chroot/etc/named.conf
重启DNS
systemctl restart named-chroot

```
## 测试是否成功
```
自己是客户端也是服务器的方法
 vim /etc/resolv.conf
 里面的地址设置成自己的服务器
 nameserver 47.115.79.9
 
```

